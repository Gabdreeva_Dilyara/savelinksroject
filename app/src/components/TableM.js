import React from 'react';
import {withRouter} from "react-router-dom";
import {API_BASE_URL, TOKEN} from "../utils/Utils";
import axios from 'axios';
import ModalAddLink from "./ModalAddLink";

class TableM extends React.Component {
    constructor(props) {
        super(props);
        this.state= {
            links: [],
            modal: false,
        };
        this.loadLinks = this.loadLinks.bind(this);
        this.toggle = this.toggle.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }
    componentDidMount(){
        this.loadLinks();
    }
    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }
    loadLinks () {
        const { id } = this.props.match.params;
        const url = '/folder/' + id;
        if (!localStorage.getItem(TOKEN)) {
            return Promise.reject("No access token set");
        }
        let headers = {
            'Content-Type': 'application/json'
        };
        if (localStorage.getItem(TOKEN)) {
            headers = Object.assign({}, headers, {'Authorization': localStorage.getItem(TOKEN)});
        }
        let options = {
            method: 'GET',
            url: url,
            headers
        };

        const instance = axios.create({baseURL: API_BASE_URL});
        instance(options)
            .then(response => {
                console.log(response.data);
                if (!(response.status === 200)) {
                    return Promise.reject(response);
                }
                console.log(response.data);
                this.setState({
                    links: response.data
                });
            });
    }

    onDelete = (id) => {
        const url = "link/delete/" + id;
        if (!localStorage.getItem(TOKEN)) {
            return Promise.reject("No access token set");
        }
        let headers =  {
            'Content-Type': 'application/json'
        };
        if (localStorage.getItem(TOKEN)) {
            headers = Object.assign({}, headers, {'Authorization':  localStorage.getItem(TOKEN)});
        }
        let options = {
            method: 'GET',
            url: url,
            headers
        };

        const instance = axios.create({baseURL: API_BASE_URL});
        instance(options);
    };
    render() {
        const row = this.state.links.map((link) =>
                <tr>
                    <th scope="row">1</th>
                    <td>{link.name}</td>
                    <td><a href={link.url}>{link.url}</a></td>
                    <td>
                        <a type="button" className="btn btn-danger btn-sm" onSubmit={this.onDelete(link.id)}>Удалить</a>
                    </td>
                </tr>

        );
        return (
            <div className="section">
                <div className="container">
                    <div className=" md-3 ">
                    <button className="btn btn-success btn-md" onClick={this.toggle} >Добавить</button>
                    </div>
                    <ModalAddLink handleToggle={this.toggle} modalState={this.state.modal} folder={this.props.match.params} />
                    <table className="table table-striped table-responsive table-light table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Название</th>
                            <th scope="col">URL</th>
                            <th scope="col">Удалить</th>
                        </tr>
                        </thead>
                        <tbody>
                        {row}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}
export default withRouter(TableM);