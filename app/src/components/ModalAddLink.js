import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, Form, FormGroup } from 'reactstrap';
import {TOKEN, API_BASE_URL} from "./../utils/Utils";
import axios from 'axios';

class ModalAddLink extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            url: '',
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    };

    onSubmit = (e) => {
        e.preventDefault();
        const { name, url } = this.state;
        const folder = this.props.folder.id;
        if (!localStorage.getItem(TOKEN)) {
            return Promise.reject("No access token set");
        }
        let headers =  {
            'Content-Type': 'application/json'
        };
        if (localStorage.getItem(TOKEN)) {
            headers = Object.assign({}, headers, {'Authorization':  localStorage.getItem(TOKEN)});
        }
        let options = {
            method: 'POST',
            url: '/link/create',
            data: {
               name, url, folder
            },
            headers
        };

        const instance = axios.create({baseURL: API_BASE_URL});
        instance(options);
        window.location.reload();
    };

    onChange = (e) => {
        const state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
    };
    render() {
        return (
            <div>
                <Modal isOpen={this.props.modalState} toggle={this.props.handleToggle} className={this.props.className} backdrop='static'>
                    <ModalHeader toggle={this.props.handleToggle} className="text-center">Добавить ссылку</ModalHeader>
                    <form onSubmit={this.onSubmit}>
                    <ModalBody>
                            <div className="form-group">
                                <label>Название:</label>
                                <input name="name" type="text" placeholder="Введите название ссылки" className="form-control" value={this.state.name} onChange={this.onChange}/>
                            </div>
                        <div className="form-group">
                            <label>Ссылка:</label>
                            <input name="url" type="url" placeholder="Вставьте ссылку" className="form-control" value={this.state.url} onChange={this.onChange}/>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button className="btn-simple" type="submit" color="primary" onClick={this.props.handleToggle} >Сохранить</Button>{' '}
                        <Button className="btn-simple" color="secondary" onClick={this.props.handleToggle}>Отмена</Button>
                    </ModalFooter>
                    </form>
                </Modal>
            </div>
        );
    }
}

export default ModalAddLink;