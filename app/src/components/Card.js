import React, { Component } from 'react';

const CardImpl = (props) => {
    const color = [' bg-warning', 'bg-info', 'bg-success', 'bg-primary', 'bg-danger'];
    let rand = Math.round(0 - 0.5 + Math.random() * 5);
    const classN = "card text-white mb-3 " + color[rand];
    const id = props.id;
    const url = "/folder/"  + id;
    return (
            <div className="col-sm-3">
                <a href={url}>
                <div className={classN}>
                    <div className="card-body">
                        <h5 className="card-title">{props.title}</h5>
                    </div>
                    <div className="card-footer">Обновлено {props.date}</div>
                </div>
                </a>
            </div>

    );
};

export default CardImpl;
