import React, {Component} from 'react';

class SignIn extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="section" style={{padding: "12vh"}}>
                <div className="container">
                    <div className="row">
                        <div className="card card-signup" data-background-color="orange">
                            <form onSubmit={this.props.onSubmit}>
                                <div className="header text-center">
                                    <h4 className="title title-up">Авторизация</h4>
                                </div>
                                <div className="card-body">
                                <div className="input-group form-group-no-border input-lg">
                                <span className="input-group-addon">
                                    <i className="now-ui-icons users_circle-08"></i>
                                </span>
                                    <input type="text" className="form-control" placeholder="Логин..." name="login" value={this.props.login} onChange={this.props.onChange} />
                                </div>
                                <div className="input-group form-group-no-border input-lg">
                                <span className="input-group-addon">
                                    <i className="now-ui-icons objects_key-25"></i>
                                </span>
                                    <input type="password" placeholder="Пароль..." className="form-control" name="password" value={this.props.password} onChange={this.props.onChange} />
                                </div>
                            </div>
                                <div className="footer text-center">
                                    <button type="submit" className="btn btn-warning btn-round btn-lg">Войти</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default SignIn;
