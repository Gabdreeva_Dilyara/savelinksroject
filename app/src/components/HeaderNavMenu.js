import React, { Component } from 'react';
import {
    Collapse,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavItem,
    NavLink
} from "reactstrap";

class HeaderNavMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            dropdownOpen: false
        };
        this.toggle = this.toggle.bind(this);
        this.toggleDropdown = this.toggleDropdown.bind(this);
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    toggleDropdown() {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }
    render() {
        const user = this.props.currentUser;
        const title = user ? (
            <NavItem className="nav-item">
                <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown} className="dropdown">
                    <DropdownToggle caret className="btn btn-lg  btn-warning dropdown-toggle">
                            {user.login}
                    </DropdownToggle>
                    <DropdownMenu className="dropdown-menu">
                        <DropdownItem className="dropdown-item btn-lg" href="/profile">Профиль</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem className="dropdown-item btn-lg"  onClick={this.props.onLogout}>Выйти</DropdownItem>
                    </DropdownMenu>
                </Dropdown>
            </NavItem>
        ) : (
            <NavItem className="nav-item btn-lg">
                <NavLink className="nav-link btn btn-warning btn-lg" href="/signin">Войти</NavLink>
            </NavItem>
        );

        return (
            <Navbar className="navbar navbar-expand-lg bg-warning">
                <div className="container">
                    <div className="navbar-translate">
                        <NavbarBrand className="navbar-brand" href="/">
                            Save Links Project
                        </NavbarBrand>
                        <button className="navbar-toggler navbar-toggler-right" onClick={this.toggle}>
                            <span className="navbar-toggler-bar bar1"></span>
                            <span className="navbar-toggler-bar bar2"></span>
                            <span className="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <Collapse className="collapse navbar-collapse justify-content-end" isOpen={this.state.isOpen} navbar>
                        <Nav className="navbar-nav ml-auto" navbar>
                            {title}
                        </Nav>
                    </Collapse>
                </div>
            </Navbar>
        );
    }
}

export default HeaderNavMenu;
