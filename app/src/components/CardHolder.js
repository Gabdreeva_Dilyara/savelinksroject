import React, { Component } from 'react';
import Card from "./Card";
import NewCard from "./NewCard";

class CardHolder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date1: '31.02.12'
        }

    }
    render() {
        let folders = this.props.folders;
        let card = folders.map((folder) =>
            <Card title={folder.name} id={folder.id} date={this.state.date1}/>
        );
        return (
            <div className="row">
                {card}
                <NewCard loadFolders={this.props.loadFolders}/>
            </div>
        );
    }
}

export default CardHolder;
