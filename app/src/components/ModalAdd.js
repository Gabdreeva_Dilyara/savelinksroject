import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, Form, FormGroup } from 'reactstrap';
import {TOKEN, API_BASE_URL} from "./../utils/Utils";
import axios from 'axios';

class ModalAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: ''
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    };

    onSubmit = (e) => {
        e.preventDefault();
        const { category } = this.state;

        if (!localStorage.getItem(TOKEN)) {
            return Promise.reject("No access token set");
        }
        let headers =  {
            'Content-Type': 'application/json'
        };
        if (localStorage.getItem(TOKEN)) {
            headers = Object.assign({}, headers, {'Authorization':  localStorage.getItem(TOKEN)});
        }
        let options = {
            method: 'POST',
            url: '/category/create',
            data: {
               category
            },
            headers
        };

        const instance = axios.create({baseURL: API_BASE_URL});
        instance(options);
    };

    onChange = (e) => {
        const state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
    };
    render() {
        return (
            <div>
                <Modal isOpen={this.props.modalState} toggle={this.props.handleToggle} className={this.props.className} backdrop='static'>
                    <ModalHeader toggle={this.props.handleToggle} className="text-center">Добавить категорию</ModalHeader>
                    <form onSubmit={this.onSubmit}>
                    <ModalBody>
                            <div className="form-group">
                                <label>Название:</label>
                                <input name="category" type="text" placeholder="Введите название категории" className="form-control" value={this.state.category} onChange={this.onChange}/>
                            </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button className="btn-simple" type="submit" color="primary" onClick={this.props.handleToggle}>Сохранить</Button>{' '}
                        <Button className="btn-simple" color="secondary" onClick={this.props.handleToggle}>Отмена</Button>
                    </ModalFooter>
                    </form>
                </Modal>
            </div>
        );
    }
}

export default ModalAdd;