import React, { Component } from 'react';
import Plus from 'react-icons/lib/fa/plus-square';
import ModalAdd from "./ModalAdd";
class CardImpl extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }
    render() {
        return (
            <div className="col-sm-3">
                <ModalAdd handleToggle={this.toggle} modalState={this.state.modal} />
                <div className="card text-black text-center mb-3 bg-light">
                    <div className="card-body">
                        <h5 className="card-title">Добавить</h5>
                        <Plus onClick={this.toggle} style={{fontSize: 100, color: '#eee'}}/>
                    </div>
                    <div className="card-footer">Новая категория</div>
                </div>
            </div>

        )
    }
}

export default CardImpl;
