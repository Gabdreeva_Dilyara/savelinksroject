import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App'
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter } from 'react-router-dom';
import './css/bootstrap.min.css';
import './css/now-ui-kit.css';
import './css/demo.css';

ReactDOM.render(
    <BrowserRouter>
         <App />
    </BrowserRouter>, document.getElementById('root'));
registerServiceWorker();
