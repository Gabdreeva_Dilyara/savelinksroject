import React, { Component } from 'react';
import './App.css';
import {Route, Router, Switch, withRouter} from 'react-router-dom';
import AuthSigninPage from "./pages/AuthSigninPage";
import AuthSignupPage from "./pages/AuthSignupPage";
import MainPage from "./pages/MainPage";
import HeaderNavMenu from "./components/HeaderNavMenu";
import Footer from "./components/Footer";
import {TOKEN, API_BASE_URL} from "./utils/Utils";
import axios from 'axios';
import TableM from "./components/TableM";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: null,
            isAuthenticated: false
        };
        this.handleLogout = this.handleLogout.bind(this);
        this.loadCurrentUser = this.loadCurrentUser.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    loadCurrentUser() {
        if (!localStorage.getItem(TOKEN)) {
            return Promise.reject("No access token set");
        }
        let headers =  {
            'Content-Type': 'application/json'
        };
        if (localStorage.getItem(TOKEN)) {
            headers = Object.assign({}, headers, {'Authorization':  localStorage.getItem(TOKEN)});
        }
        let options = {
            method: 'GET',
            url: '/user/me',
            headers
        };

        const instance = axios.create({baseURL: API_BASE_URL});
        instance(options)
            .then(response => {
                if (!(response.status === 200)) {
                    return Promise.reject(response);
                }
                console.log(response.data);
                this.setState({
                    currentUser: response.data,
                    isAuthenticated: true
                });
            });
    }

    componentWillMount() {
        this.loadCurrentUser();
    }
    handleLogout() {
        localStorage.removeItem(TOKEN);
        this.setState({
            currentUser: null,
            isAuthenticated: false
        });
        this.props.history.push("/signin");
    }
    handleLogin() {
        this.loadCurrentUser();
        this.props.history.push("/");
    }
  render() {
    return (
        <div className="profile-page bootstrap-collapse">
            <HeaderNavMenu isAuthenticated={this.state.isAuthenticated}
                           currentUser={this.state.currentUser}
                           onLogout={this.handleLogout}
            />
            <Switch>
                <Route path="/signin" render={(props) => <AuthSigninPage onLogin={this.handleLogin} {...props} />} />
                <Route path="/signup" component={AuthSignupPage} />
                <Route path="/folder/:id" component={TableM}/>
                <Route exact path="/" render={(props) => <MainPage isAuthenticated={this.state.isAuthenticated} {...props} />}  />
            </Switch>
        </div>
    );
  }
}

export default withRouter(App);
