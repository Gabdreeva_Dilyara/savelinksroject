import React, { Component } from 'react';
import {TOKEN, API_BASE_URL} from "../utils/Utils";
import axios from 'axios';
import SignIn from "../components/SignIn";
import {withRouter} from "react-router-dom";

class AuthSigninPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit = (e) => {
        e.preventDefault();
        const { login, password } = this.state;
        let options = {
            method: 'POST',
            url: "/auth/signin",
            data: {
                login, password
            },
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const instance = axios.create({baseURL: API_BASE_URL});
        instance(options)
            .then(response => {
                if (!(response.status === 200)) {
                    return Promise.reject(response)
                }
                console.log(response.data.token);
                localStorage.setItem(TOKEN, response.data.token);
                this.props.onLogin();
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    onChange = (e) => {
        const state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    render() {
        return (
            <SignIn login={this.state.login} password={this.state.password} onSubmit={this.onSubmit} onChange={this.onChange}/>
        );
    }
}

export default withRouter(AuthSigninPage);
