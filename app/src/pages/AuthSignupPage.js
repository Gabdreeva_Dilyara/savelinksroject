import React, { Component } from 'react';
import {TOKEN, API_BASE_URL} from "./../utils/Utils";
import axios from 'axios';
import SignUp from "../components/SignUp";
import {withRouter} from "react-router-dom";

class AuthSignupPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            email: ''
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    };

    onSubmit = (e) => {
        e.preventDefault();
        const { login, password, email } = this.state;

        let options =  {
            method: 'POST',
            url:"/auth/signup",
            data: {
                login,password, email
            },
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const instance = axios.create({baseURL: API_BASE_URL});
        instance(options)
            .then(res => {
                console.log(res.data);
                localStorage.setItem("token", res.data);
            })
            .catch(function (error) {
                console.log(error);
            });
        this.props.history.push("/signin");
    };

    onChange = (e) => {
        const state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
    };
    render() {
        return (
            <SignUp login={this.state.login} password={this.state.password} email={this.state.email} onChange={this.onChange} onSubmit={this.onSubmit}/>
        );
    }
}

export default withRouter(AuthSignupPage);
