import React, { Component } from 'react';
import CardHolder from "../components/CardHolder";
import {withRouter} from "react-router-dom";
import {API_BASE_URL, TOKEN} from "../utils/Utils";
import axios from 'axios';

class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state= {
            folders: [],
            isLoading: true
        };
        this.loadPage = this.loadPage.bind(this);
        this.loadFolders = this.loadFolders.bind(this);
    }
    loadPage(){
        this.loadFolders()
    }
    componentWillMount(){
        if (!(localStorage.getItem(TOKEN))) {
            this.props.history.push("/signin");
        }
    }
    componentDidMount(){
        this.loadPage();
    }
    loadFolders(){
        if (!localStorage.getItem(TOKEN)) {
            return Promise.reject("No access token set");
        }
        let headers =  {
            'Content-Type': 'application/json'
        };
        if (localStorage.getItem(TOKEN)) {
            headers = Object.assign({}, headers, {'Authorization':  localStorage.getItem(TOKEN)});
        }
        let options = {
            method: 'GET',
            url: '/folders',
            headers
        };

        const instance = axios.create({baseURL: API_BASE_URL});
        instance(options)
            .then(response => {
                if (!(response.status === 200)) {
                    return Promise.reject(response);
                }
                console.log(response.data);
                this.setState({
                   folders: response.data
                });
            });

    }
    render() {
        return (
                <div className="section">
                    <div className="container">
                    <CardHolder folders={this.state.folders} loadFolders={this.loadFolders}/>
                </div>
            </div>
        );
    }
}

export default withRouter(MainPage);
