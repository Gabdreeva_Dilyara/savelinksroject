package ru.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.dto.AllFoldersResponse;
import ru.itis.dto.UserSummary;
import ru.itis.models.Folder;
import ru.itis.service.FolderService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 22.05.2018
 */

@RestController
@RequestMapping("/api")
public class FolderController {

    @Autowired
    private FolderService folderService;

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/folders")
    public ResponseEntity<?> getFolders() {
        List<Folder> folders = folderService.getAllFolders();
        List<AllFoldersResponse> foldersResponses = new ArrayList<>();
        for (Folder folder: folders) {
            foldersResponses.add(AllFoldersResponse.builder()
            .id(String.valueOf(folder.getId()))
            .name(folder.getCategory().getName())
            .build());
        }
        return ResponseEntity.ok(foldersResponses);
    }
}
