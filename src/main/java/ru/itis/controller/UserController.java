package ru.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.dto.UserSummary;
import ru.itis.models.User;
import ru.itis.service.UserService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/user/me")
    public UserSummary getCurrentUser() {
        User user = userService.getAuthenticatedUser();
        if (user == null) {
            throw new BadCredentialsException("bad credentials");
        }
        UserSummary userSummary = UserSummary.builder()
               .login(user.getLogin())
                .email(user.getEmail())
                .build();
        return userSummary;
    }
    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/users")
    public List<UserSummary> getUsers() {
        List<User> users = userService.getAll();
        List<UserSummary> userSummaries = new ArrayList<>();
        for (User user: users) {
            userSummaries.add(UserSummary.builder()
                    .login(user.getLogin())
                    .email(user.getEmail())
                    .build());
        }

        return userSummaries;
    }
}
