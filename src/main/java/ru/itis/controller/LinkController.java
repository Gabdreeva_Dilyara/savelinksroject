package ru.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.stylesheets.LinkStyle;
import ru.itis.dto.AllLinksResponse;
import ru.itis.dto.LinkRequest;
import ru.itis.models.Link;
import ru.itis.service.LinksService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 22.05.2018
 */

@RestController
@RequestMapping("/api")
public class LinkController {

    @Autowired
    private LinksService linksService;

    @GetMapping("/folder/{id}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<?> getLinks(@PathVariable("id") String id){
        Set<Link> linkSet = linksService.getAllLinks(Long.parseLong(id));
        List<AllLinksResponse> linksResponses = new ArrayList<>();
        for (Link link: linkSet) {
            linksResponses.add(AllLinksResponse.builder()
                    .name(link.getName())
                    .url(link.getUrl())
                    .id(link.getId())
                    .build());
        }
        return ResponseEntity.ok(linksResponses);
    }
    @PostMapping("/link/create")
    @PreAuthorize("hasAuthority('USER')")
    public void createLink(@RequestBody LinkRequest linkRequest) {
        linksService.saveLink(linkRequest);
    }
    @Transactional
    @GetMapping("/link/delete/{id}")
    @PreAuthorize("hasAuthority('USER')")
    public void deleteLink(@PathVariable("id") String id) {
        linksService.deleteLink(id);
    }
}
