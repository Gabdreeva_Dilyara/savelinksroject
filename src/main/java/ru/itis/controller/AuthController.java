package ru.itis.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.dto.ApiResponse;
import ru.itis.dto.LoginRequest;
import ru.itis.dto.SignUpRequest;
import ru.itis.dto.TokenResponse;
import ru.itis.service.AuthService;

/**
 *
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/signin")
    @PreAuthorize("permitAll()")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) {
        TokenResponse jwt = authService.login(loginRequest);
        return ResponseEntity.ok(jwt);
    }

    @PostMapping("/signup")
    @PreAuthorize("permitAll()")
    public ResponseEntity<?> registerUser(@RequestBody SignUpRequest signUpRequest) {
        ApiResponse apiResponse = authService.register(signUpRequest);
        if (!apiResponse.getSuccess()) {
            return ResponseEntity.badRequest().body(apiResponse);
        }
        return ResponseEntity.ok(apiResponse);
    }
}
