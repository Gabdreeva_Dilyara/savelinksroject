package ru.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.CategoryRequest;
import ru.itis.models.Folder;
import ru.itis.service.CategoryService;
import ru.itis.service.FolderService;
import ru.itis.service.UserService;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 22.05.2018
 */

@RestController
@RequestMapping("/api")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private FolderService folderService;

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping("/category/create")
    public void createCategory(@RequestBody CategoryRequest categoryRequest) {
        categoryService.createCategory(categoryRequest.getCategory());
        folderService.createFolder(categoryRequest.getCategory(), userService.getAuthenticatedUser());
    }
}
