package ru.itis.dto;

import lombok.*;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SignUpRequest {

    private String login;
    private String email;
    private String password;
}
