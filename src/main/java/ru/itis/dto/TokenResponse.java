package ru.itis.dto;

import lombok.*;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 18.05.2018
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TokenResponse {
    private String token;
}
