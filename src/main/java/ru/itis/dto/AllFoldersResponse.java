package ru.itis.dto;

import lombok.*;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 22.05.2018
 */


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AllFoldersResponse {
    private String name;
    private String id;
}
