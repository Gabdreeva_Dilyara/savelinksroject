package ru.itis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.Folder;
import ru.itis.models.User;
import ru.itis.repositories.CategoryRepository;
import ru.itis.repositories.FolderRepository;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 22.05.2018
 */

@Service
public class FolderService {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UserService userService;

    @Autowired
    private FolderRepository folderRepository;

    public void createDefaultFolders(User user){

      createFolder("Развлечения", user);
      createFolder("Хобби", user);
      createFolder("Учеба", user);
      createFolder("Личное", user);
      createFolder("Неотсортированное", user);
    }
    public void createFolder(String name, User user){

        Folder folder = Folder.builder()
                .category(categoryService.getByName(name))
                .user(user)
                .build();

        folderRepository.save(folder);

    }

    public List<Folder> getAllFolders() {
        User user = userService.getAuthenticatedUser();
        return folderRepository.getAllByUser(user);
    }
}
