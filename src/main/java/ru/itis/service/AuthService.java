package ru.itis.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.dto.ApiResponse;
import ru.itis.dto.LoginRequest;
import ru.itis.dto.SignUpRequest;
import ru.itis.dto.TokenResponse;
import ru.itis.models.Category;
import ru.itis.models.Role;
import ru.itis.models.User;
import ru.itis.repositories.UserRepository;

import java.util.Date;
import java.util.Optional;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 17.05.2018
 */

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private FolderService folderService;

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private Long jwtExpirationInMs;



    public TokenResponse login(LoginRequest loginRequest) {
        String rawPassword = loginRequest.getPassword();
        String login = loginRequest.getLogin();

        Optional<User> optionalUser = userRepository.findByLogin(login);
        if (!optionalUser.isPresent()) {
            throw new IllegalArgumentException("User login/password incorrect");
        }
        User user = optionalUser.get();
        Date date = new Date();
        Long expire = date.getTime() + jwtExpirationInMs;

        if (passwordEncoder.matches(rawPassword, user.getPassword())) {
                return TokenResponse.builder().token(Jwts.builder()
                        .claim("role", user.getRole().toString())
                        .claim("login", user.getLogin())
                        .setSubject(user.getId().toString())
                        .setExpiration(new Date(expire))
                        .signWith(SignatureAlgorithm.HS512, jwtSecret)
                        .compact())
                        .build();
        }
        throw new IllegalArgumentException("User login/password incorrect");
    }


    public ApiResponse register(SignUpRequest signUpRequest) {

        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ApiResponse(false, "Email Address already in use!");
        }


        User user = User.builder()
                .login(signUpRequest.getLogin())
                .email(signUpRequest.getEmail())
                .password(passwordEncoder.encode(signUpRequest.getPassword()))
                .role(Role.USER)
                .build();

        userRepository.save(user);
        folderService.createDefaultFolders(user);

        return new ApiResponse(true, "Пользователь зарегистрирован");
    }
}
