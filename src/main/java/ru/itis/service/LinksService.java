package ru.itis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.dto.LinkRequest;
import ru.itis.models.Folder;
import ru.itis.models.Link;
import ru.itis.models.User;
import ru.itis.repositories.FolderRepository;
import ru.itis.repositories.LinkRepository;

import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 22.05.2018
 */

@Service
public class LinksService {

    @Autowired
    private LinkRepository linkRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private FolderRepository folderRepository;

    public Set<Link> getAllLinks(Long id) {
        Folder folder = folderRepository.findById(id);
        Set<Link> links = folder.getLink();
        return links;

    }
    public void saveLink(LinkRequest linkRequest) {
        Folder folder = folderRepository.findById(Long.parseLong(linkRequest.getFolder()));
        Link link = Link.builder()
                .name(linkRequest.getName())
                .url(linkRequest.getUrl())
                .folder(folder)
                .build();
        linkRepository.save(link);
    }
    public void deleteLink(String id) {
        Link link = linkRepository.findById(Long.parseLong(id));
        link.setFolder(null);
        linkRepository.removeLinkById(Long.parseLong(id));
    }
}
