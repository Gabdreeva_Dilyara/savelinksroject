package ru.itis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import ru.itis.exception.ResourceNotFoundException;
import ru.itis.models.Category;
import ru.itis.repositories.CategoryRepository;

import java.util.Optional;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 22.05.2018
 */

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public Category getByName(String name) {
        Optional<Category> category = categoryRepository.getByName(name);
        if (category.isPresent()) {
            return category.get();
        }
        throw new ResourceNotFoundException("Категория", name, name);
    }
    public void createCategory(String name) {
        Category category = Category.builder()
                .name(name)
                .build();
        categoryRepository.save(category);
    }
}
