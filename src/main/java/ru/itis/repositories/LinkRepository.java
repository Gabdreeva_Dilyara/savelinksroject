package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Folder;
import ru.itis.models.Link;

public interface LinkRepository extends JpaRepository<Link, Long> {
    void deleteLinkById(Long id);
    Link findById(Long id);
    void removeLinkById(Long id);
}
