package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Folder;
import ru.itis.models.User;

import java.util.List;

public interface FolderRepository extends JpaRepository<Folder, Long> {

    List<Folder> getAllByUser(User user);
    Folder findById(Long id);
}
