package ru.itis.models;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 22.05.2018
 */

@Entity
@Table
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Folder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn
    private Category category;

    @OneToMany(mappedBy = "folder", cascade = CascadeType.ALL)
    private Set<Link> link;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private User user;


}
