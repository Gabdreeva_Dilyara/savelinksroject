package ru.itis.models;

import lombok.*;

import javax.persistence.*;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 22.05.2018
 */

@Entity
@Table
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToOne(mappedBy = "category")
    private Folder folder;


}
