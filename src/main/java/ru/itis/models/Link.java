package ru.itis.models;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 * 11-602
 * 22.05.2018
 */

@Entity
@Table
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Link {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String url;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Folder folder;
}
