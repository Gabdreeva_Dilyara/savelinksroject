package ru.itis.utils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Time;
import java.time.LocalTime;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Converter(autoApply = true)
public class LocalTimeAttributeConverter implements AttributeConverter<LocalTime, Time>{
    @Override
    public Time convertToDatabaseColumn(LocalTime time) {
        return (time == null ? null : Time.valueOf(time));
    }

    @Override
    public LocalTime convertToEntityAttribute(Time time) {
        return (time == null? null : time.toLocalTime());
    }
}
